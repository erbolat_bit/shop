<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('stores', function (Blueprint $table) {
      $table->id();
      $table->unsignedBigInteger('user_id');
      $table->string('title');
      $table->string('subdomain')->nullable();
      $table->string('logotype')->nullable();
      $table->smallInteger('type')->comment('1 со складом, 2 виртуальный, 3 режим каталога');
      $table->json('delivery')->nullable();
      $table->text('description')->nullable();
      $table->string('bg_image')->nullable();
      $table->json('bg_color')->nullable();
      $table->integer('views')->default(0);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('stores');
  }
}
