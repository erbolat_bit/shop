<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('store_id');
            $table->integer('category_id');
            $table->integer('brand_id')->nullable();
            $table->string('title');
            $table->integer('price');
            $table->integer('discount_price')->nullable();
            $table->integer('currency_id');
            $table->integer('quantity');
            $table->integer('quantity_size');
            $table->text('description')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('manufacturer_id')->nullable();
            $table->text('detailed_description')->nullable();
            $table->text('delivery_description')->nullable();
            $table->integer('views')->default(0);
            $table->timestamps();


            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
