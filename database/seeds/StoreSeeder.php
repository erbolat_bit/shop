<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StoreSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faker = Faker\Factory::create();
    $stores = [];
    for ($i = 0; $i < 100; $i++) {
      $stores[] = [
        'user_id' => rand(142, 150),
        'title' => $faker->company(),
        'subdomain' => $faker->companySuffix(),
        'logotype' => $faker->catchPhrase(),
        'type' => rand(1, 3),
        'description' => $faker->text(300),
        'bg_image' => $faker->text(10) . 'png',
        'views' => rand(1, 10000000),
        'created_at' => $faker->dateTime()
      ];
    }
    DB::table('stores')->insert($stores);
  }
}
