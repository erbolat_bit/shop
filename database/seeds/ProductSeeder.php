<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faker = Faker\Factory::create();
    $products = [];
    for ($i = 0; $i < 500; $i++) {
      $products[] = [
        'store_id' => rand(3, 140),
        'category_id' => rand(11, 30),
        'title' => $faker->company(),
        'price' => rand(1000, 9999999),
        'discount_price' => rand(400, 1000),
        'currency_id' => rand(1, 3),
        'quantity' => rand(10, 99),
        'quantity_size' => rand(1, 10),
        'description' => $faker->randomHtml(2, 3),
        'country_id' =>  rand(1, 250),
        'manufacturer_id' => rand(7, 27),
        'detailed_description' => $faker->randomHtml(3, 4),
        'delivery_description' => $faker->randomHtml(1, 2),
        'views' => rand(1, 1000),
        'created_at' => $faker->dateTime('now')
      ];
    }
    DB::table('products')->insert($products);
  }
}
