<?php
return [
  'app_name' => 'Buynow',
  'url' => env('FRONTEND_URL', 'http://localhost:8080'),
  // path to my frontend page with query param queryURL(temporarySignedRoute URL)
  'email_verify_url' => env('FRONTEND_EMAIL_VERIFY_URL', 'email/verify?queryURL='),
  'reset_password' => env('FRONTEND_RESET_PASSWORD_URL', 'reset/password?queryURL='),

  'currencies' => [
    1 => 'UZS',
    2 => 'USD',
    3 => 'RUB'
  ],

  'weights' => [
    'kg' => 'Кг',
    'st' => 'Цт',
    'tn' => 'Тн',
  ],

  'type_inputs' => [
    'text' => 'Input text',
    // 'textarea' => 'Textarea',
    'select' => 'Select',
    'checkbox' => 'Checkbox',
    // 'radio' => 'Radio',
  ],

  'paymnet_types' => [
    1 => 'Click',
    2 => 'Payme',
    3 => 'Upay',
    4 => 'Paynet'
  ],

  'quantity_size' => [
    1 => 'комлектов',
    2 => 'шт'
  ],

  'delivery_type' => [
    1 => 'По району',
    2 => 'По городу',
    3 => 'По стране',
    4 => 'По миру',
  ],

  'who_is' => [
    1 => 'Физ. лицо',
    2 => 'Юр. лицо',
    3 => 'Гос. организация',
  ],

  'store_types' => [
    1 => 'со складом',
    2 => 'виртуальный',
    3 => 'режим каталога'
  ]

];
