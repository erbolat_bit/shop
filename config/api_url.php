<?php

return [
  'auth' => 'https://auth.uzexpress.ru',
  'gateway' => 'https://gateway.uzexpress.ru',
  'reviews' => 'https://reviews.uzexpress.ru'
];