<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;

class Store extends Model
{

  protected $fillable = [
    'logotype',
    'title',
    'description',
    'type',
    'subdomain',
    'delivery',
    'bg_color',
    'bg_image',
    'user_id',
    // 'views',
    'status'
  ];

  protected $casts = [
    'delivery' => 'array',
    'bg_color' => 'array'
  ];


  protected static function boot()
  {
    parent::boot();

    self::created(function ($model) {
      Artisan::call('clear:server-cache');
    });
    self::updated(function ($model) {
      Artisan::call('clear:server-cache');
    });
    self::deleted(function ($model) {
      Artisan::call('clear:server-cache');
    });
    self::deleting(function ($model) {
      $model->products()->delete();
    });
  }

  // public function user(){
  //     return $this->belongsTo('App\User');
  // }

  public function addData()
  {
    return $this->hasMany('App\Models\StoreAddData')
      ->orderBy('main', 'desc');
  }

  public function products()
  {
    return $this->hasMany('App\Models\Product');
  }

  public function scopeActive($query)
  {
    return $query->where('status', true);
  }

  public function scopeInActive($query)
  {
    return $query->where('status', false);
  }

  public function viewsCount()
  {
    return $this->morphMany(View::class, 'viewable');
  }

}
