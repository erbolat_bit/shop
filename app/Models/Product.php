<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{

  protected $fillable = [
    'store_id',
    'category_id',
    'brand_id',
    'title',
    'price',
    'discount_price',
    'currency_id',
    'quantity',
    'quantity_size',
    'description',
    'country_id',
    'manufacturer_id',
    'detailed_description',
    'delivery_description',
    // 'views',
  ];

  protected static function boot()
  {
    parent::boot();

    self::created(function ($model) {
      Artisan::call('clear:server-cache');
    });

    self::updated(function ($model) {
      Artisan::call('clear:server-cache');
    });

    self::deleted(function ($model) {
      Artisan::call('clear:server-cache');
    });
    self::deleting(function ($model) {
      $products = $model->with(['photos', 'docs'])->get()->toArray();
      $credentials = [];
      if (array_key_exists('photos', $products)) {
        foreach ($products['photos'] as $value) {
          $credentials = $value['photo'];
        }
      }
      if (array_key_exists('docs', $products)) {
        foreach ($products['docs'] as $doc) {
          $credentials = $doc['document'];
        }
      }
      Storage::disk('public')->delete($credentials);
      $model->photos()->delete();
      $model->docs()->delete();
      $model->params()->delete();
      $model->tags()->delete();
    });
  }

  public function store()
  {
    return $this->belongsTo('App\Models\Store');
  }

  public function currency()
  {
    return config('params.currencies')[$this->currency_id];
  }

  public function photos()
  {
    return $this->hasMany('App\Models\ProductPhoto');
  }

  public function tags()
  {
    return $this->hasMany('App\Models\ProductTag');
  }

  public function docs()
  {
    return $this->hasMany('App\Models\ProductDoc');
  }

  public function params()
  {
    return $this->hasMany('App\Models\ProductParam');
  }

  public function scopePopular($query, $desc)
  {
    return $query;
  }

  public function viewsCount()
  {
    return $this->morphMany(View::class, 'viewable');
  }

  public function viewsCountShop()
  {
    return $this->morphMany(View::class, 'viewable')->where('has_shop', 1);
  }

  public function viewsCountAd()
  {
    return $this->morphMany(View::class, 'viewable')->where('has_ad', 1);
  }

  public function viewsCountLot()
  {
    return $this->morphMany(View::class, 'viewable')->where('has_lot', 1);
  }

  public function userViewsSum()
  {
    return $this->viewsCount->sum('view');
  }
}
