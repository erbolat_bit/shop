<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;

class ProductParam extends Model
{

  protected $fillable = [
    'option_id',
    'product_id',
    'value',
    'parameter_id',
    'type'
  ];

  public $timestamps = false;


  protected static function boot()
  {
    parent::boot();

    self::created(function ($model) {
      Artisan::call('clear:server-cache');
    });

    self::updated(function ($model) {
      Artisan::call('clear:server-cache');
    });

    self::deleted(function ($model) {
      Artisan::call('clear:server-cache');
    });
  }
}
