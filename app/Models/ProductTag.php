<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;

class ProductTag extends Model
{

  protected $fillable = [
    'product_id',
    'title',
  ];

  public $timestamps = false;

  public function product()
  {
    return $this->belongsTo('App\Models\Product');
  }

  protected static function boot()
  {
    parent::boot();

    self::created(function ($model) {
      Artisan::call('clear:server-cache');
    });

    self::updated(function ($model) {
      Artisan::call('clear:server-cache');
    });

    self::deleted(function ($model) {
      Artisan::call('clear:server-cache');
    });
  }
}
