<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;

class StoreAddData extends Model
{

  protected $table = 'store_add_data';

  protected $fillable = [
    'title',
    'category',
    'main',
  ];

  public $timestamps = false;

  protected static function boot()
  {
    parent::boot();

    self::created(function ($model) {
      Artisan::call('clear:server-cache');
    });

    self::updated(function ($model) {
      Artisan::call('clear:server-cache');
    });

    self::deleted(function ($model) {
      Artisan::call('clear:server-cache');
    });
  }

  public function store()
  {
    return $this->belongsTo('App\Models\Api\User\Store\Store');
  }
}
