<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{

  protected $fillable = [
    'user_id',
    'viewable_id',
    'viewable_type',
    'view',
    'has_shop',
    'has_ad',
    'has_lot'
  ];

  protected $casts = [
    'has_shop' => 'boolean',
    'has_ad' => 'boolean',
    'has_lot' => 'boolean',
  ];

  public function viewable()
  {
    return $this->morphTo();
  }
}