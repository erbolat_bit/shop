<?php

namespace App\Invoke;

use Illuminate\Support\Facades\Storage;

class CbuCurrencies
{
  
  public function __invoke()
  {
    $currencies = file_get_contents(config('app.currency_url'));
    Storage::put('currency/currencies.json', $currencies);
  }
}
