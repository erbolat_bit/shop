<?php

namespace App\Services;

use App\Models\Store;
use App\Models\View;
use App\Repositories\ProductRepository;
use App\Repositories\ShopRepository;

class ViewsService
{

  private $shopRepo;
  private $productRepo;

  public function __construct(ShopRepository $shopRepository, ProductRepository $productRepository)
  {
    $this->shopRepo = $shopRepository;
    $this->productRepo = $productRepository;
  }

  public function getAllShopViewsSum($params)
  {
    $stores = $this->shopRepo->allShopViewsSum($params);
    $data = $this->countViews($stores);

    return $data;
  }

  public function getShopViews($id, $query_params)
  {
    $model = $this->shopRepo->getShopViews($id, $query_params);
    return $model->toArray();
  }

  public function getAllProductViewsSum($params)
  {
    $products = $this->productRepo->allProductViewsSum($params);
    $data = $this->countViews($products);

    return $data;
  }

  public function getProductViewsByShop($shop_d, $query_params)
  {
    $models = $this->productRepo->getProductsViewsByShop($shop_d, $query_params);
    $data = $this->countViews($models);

    return $data;
  }

  public function getViewStatistic($shop_id=null, $params=[])
  {
    if (!$shop_id) {
      $data['shop'] = $this->getAllShopViewsSum($params);
      $data['product'] = $this->getAllProductViewsSum($params);
      $data['user_ids'] = $this->userIds();
    }else{
      $data['user_ids'] = $this->userIds($shop_id);
      $data['shop'] = $this->getShopViews($shop_id, $params);
      $data['product'] = $this->getProductViewsByShop($shop_id, $params);
    }
    return $data;
  }

  private function userIds($shop_id=null)
  {
    $query = new View();
    if ($shop_id) {
        $views = $query->where('viewable_id', $shop_id)->where('viewable_type', Store::class)->whereNotNull('user_id')->distinct('user_id')->pluck('user_id');
    }else{
      $views = $query->whereNotNull('user_id')->distinct('user_id')->pluck('user_id');
    }
    return $views->toArray();
  }

  private function countViews($models){
    $data = [
      'views' => 0,
      'auth_user_views' => 0,
      'auth_user_has_shop_views' => 0,
      'auth_user_has_ad_views' => 0,
      'auth_user_has_lot_views' => 0,
    ];
    foreach ($models as $key => $model) {
      $data['views'] += $model->views;
      $data['auth_user_views'] += $model->auth_user_views;
      $data['auth_user_has_shop_views'] += $model->auth_user_has_shop_views;
      $data['auth_user_has_ad_views'] += $model->auth_user_has_ad_views;
      $data['auth_user_has_lot_views'] += $model->auth_user_has_lot_views;
    }
    $data['models'] = $models->toArray();

    return $data;
  }
}
