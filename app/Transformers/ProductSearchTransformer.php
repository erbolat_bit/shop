<?php

namespace App\Transformers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\DataArraySerializer;
use League\Fractal\TransformerAbstract;

class ProductSearchTransformer extends TransformerAbstract
{

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Collection $data)
    {
        return [
            'product' => fractal()
                                ->collection($data['products'], new ProductTransformer($this->request), 'data')
                                // ->parseIncludes('favorite')
                                ->paginateWith(new IlluminatePaginatorAdapter($data['pagination']))
                                ->serializeWith(new DataArraySerializer),
            'params' => $data['params']
        ];
    }
}
