<?php

namespace App\Transformers;

use App\Currency\Convert;
use App\Models\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{

  protected $currency;
  protected $request;

  public function __construct($request = null, $currency = null)
  {
    $this->currency = $currency;
    $this->request = $request;
  }

  /**
   * List of resources to automatically include
   *
   * @var array
   */
  protected $defaultIncludes = [
    'param_options',
    'photos',
    'tags',
    'documents',
    // 'user'
  ];

  /**
   * List of resources possible to include
   *
   * @var array
   */
  protected $availableIncludes = [
    'store',
  ];

  /**
   * A Fractal transformer.
   *
   * @return array
   */
  public function transform(Product $product)
  {
    $money = new Convert();
    $currency = $this->currency ? $this->currency : config('app.locale_currency');


    $data = [
      'id' => (int) $product->id,
      'category_id' => (int) $product->category_id,
      'store_id' => (int) $product->store_id,
      'title' => $product->title,
      'price' => $product->price,
      'discount_price' => $product->discount_price,
      'price_to_basket' => $product->price ? $money->fromTo($product->price, $product->currency(), $currency)->format(true) : null,
      'convert_price' => $product->price ? $money->fromTo($product->price, $product->currency(), $currency)->format(false) : null,
      'convert_discount_price' => $product->discount_price ? $money->fromTo($product->discount_price, $product->currency(), $currency)->format(false) : null,
      'currency' => ['id' => $product->currency_id, 'title' => $product->currency()],
      'quantity' => $product->quantity,
      'quantity_size' => $product->quantity_size,
      'delivery_description' => $product->delivery_description,
      'description' => $product->description,
      'country_id' => (int) $product->country_id,
      'manufacturer_id' => (int) $product->manufacturer_id,
      'detailed_description' => $product->detailed_description,
    ];
    if ($product->views) {
      $data['views'] = $product->views;
    }

    return $data;
  }

  public function includeParamOptions(Product $product)
  {
    return $this->collection($product->params, new ParamTransformer);
  }

  public function includeStore(Product $product)
  {
    return $this->item($product->store, new StoreTransforemer);
  }

  public function includePhotos(Product $product)
  {
    return $this->collection($product->photos, new PhotoTransformer);
  }

  public function includeTags(Product $product)
  {
    return $this->collection($product->tags, new TagTransformer);
  }

  public function includeDocuments(Product $product)
  {
    return $this->collection($product->docs, new DocumentTransformer);
  }
}
