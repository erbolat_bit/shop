<?php

namespace App\Transformers;

use App\Models\Store;
use App\Models\StoreAddData;
use League\Fractal\TransformerAbstract;

class AdminStoreTransforemer extends TransformerAbstract
{

  public function __construct()
  {
  }

  /**
   * A Fractal transformer.
   *
   * @return array
   */
  public function transform(Store $store)
  {
    $data = [
      'id' => (int) $store->id,
      'user_id' => (int) $store->user_id,
      'title' => $store->title,
      'description' => $store->description,
      'logotype' => $store->logotype,
      'type' => config('params.store_types.' . $store->type),
      'subdomain' => $store->subdomain,
      'delivery' => $store->delivery,
      'bg_color' => $store->bg_color,
      'bg_image' => $store->bg_image,
      'views' => $store->views
    ];
    if ($store->product_count)
      $data['product_count'] = $store->product_count;

    return $data;
  }
}
