<?php

namespace App\Transformers;

use App\Models\Store;
use App\Models\StoreAddData;
use League\Fractal\TransformerAbstract;

class StoreTransforemer extends TransformerAbstract
{

  public function __construct()
  {

  }

  /**
   * List of resources to automatically include
   *
   * @var array
   */
  protected $defaultIncludes = [
    'data',
  ];


  /**
   * A Fractal transformer.
   *
   * @return array
   */
  public function transform(Store $store)
  {
    $data = [
      'id' => (int) $store->id,
      'user_id' => (int) $store->user_id,
      'title' => $store->title,
      'description' => $store->description,
      'logotype' => $store->logotype,
      'type' => $store->type,
      'subdomain' => $store->subdomain,
      'delivery' => $store->delivery,
      'bg_color' => $store->bg_color,
      'bg_image' => $store->bg_image,
      'product_count' => $store->products()->count(),
    ];
    if ($store->views) {
      $data['views'] = $store->views;
    }
    return $data;
  }

  public function includeData(Store $store)
  {

    return $this->collection($store->addData, function (StoreAddData $data) {
      return [
        'title' => $data->title,
        'category' => $data->category,
        'main' => $data->main
      ];
    });
  }
}
