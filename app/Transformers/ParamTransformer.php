<?php

namespace App\Transformers;

use App\Models\ProductParam;
use League\Fractal\TransformerAbstract;

class ParamTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ProductParam $item)
    {
        return [
            'id' => (int) $item->option_id,
            'parameter_id' => (int) $item->parameter_id,
            // 'title' => $item->title(),
            'value' => $item->value,
            'type' => $item->type
        ];
    }
}
