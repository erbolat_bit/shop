<?php

namespace App\Transformers;

use App\Models\ProductDoc;
use League\Fractal\TransformerAbstract;

class DocumentTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ProductDoc $document)
    {
        return [
            'id' => (int) $document->id,
            'title' => $document->title,
            'document' => $document->document,
            'size' => $document->size
        ];
    }
}
