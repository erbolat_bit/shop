<?php

namespace App\Transformers;

use App\Models\ProductPhoto;
use League\Fractal\TransformerAbstract;

class PhotoTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
      
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ProductPhoto $photo)
    {
        return [
          'id' => (int) $photo->id,
          'photo' => $photo->photo
        ];
    }
}
