<?php

namespace App\Transformers;

use App\Models\Store;
use App\Models\StoreAddData;
use League\Fractal\TransformerAbstract;

class AdminUpdateStoreTransformer extends TransformerAbstract
{

  public function __construct()
  {
  }

  /**
   * A Fractal transformer.
   *
   * @return array
   */
  public function transform(Store $store)
  {
    $data = [
      'id' => (int) $store->id,
      'title' => $store->title,
      'description' => $store->description,
      'logotype' => $store->logotype,
      'type' => config('params.store_types.' . $store->type),
      'type_list' => config('params.store_types'),
      'subdomain' => $store->subdomain,
      'delivery' => $store->delivery,
      'bg_color' => $store->bg_color,
      'bg_image' => $store->bg_image,
      'status' => $store->status
    ];

    return $data;
  }
}
