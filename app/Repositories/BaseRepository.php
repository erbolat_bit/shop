<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{

  protected $model;

  public function __construct()
  {
    $this->model = app($this->getModel()); 
  }

  abstract function getModel(): string;

  public function init(): Model
  {
    return clone $this->model;
  }

  public function getOne(?int $id)
  {
    if ($id) {
      $model = $this->init()->findOrFail($id);
    }else{
      $model = $this->init()->inRandomOrder()->first();
    }
    return $model;
  }
}