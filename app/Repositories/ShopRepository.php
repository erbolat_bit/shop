<?php

namespace App\Repositories;

use App\Models\Store;
use Illuminate\Support\Facades\DB;

class ShopRepository extends BaseRepository
{

  public function getModel(): string
  {
    return Store::class;
  }

  public function allShopViewsSum($params)
  {
    $views = $this->init()
                  ->select('id')
                  ->withCount([
                    'viewsCount as views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                    },
                    'viewsCount as auth_user_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->whereNotNull('user_id');
                    },
                    'viewsCount as auth_user_has_shop_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_shop', 1);
                    },
                    'viewsCount as auth_user_has_ad_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_ad', 1);
                    },
                    'viewsCount as auth_user_has_lot_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_lot', 1);
                    }
                  ])
                  ->get();
    
    return $views;
  }

  public function getShopViews(int $id, $params)
  {
    $model = $this->init()
                  ->select([
                    'id',
                  ])
                  ->where('id', $id)
                  ->withCount([
                    'viewsCount as views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                    },
                    'viewsCount as auth_user_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->whereNotNull('user_id');
                    },
                    'viewsCount as auth_user_has_shop_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_shop', 1);
                    },
                    'viewsCount as auth_user_has_ad_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_ad', 1);
                    },
                    'viewsCount as auth_user_has_lot_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_lot', 1);
                    }
                  ])
                  ->first();
    return $model;
  }

}