<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{

  public function getModel(): string
  {
    return Product::class;
  }

  public function allProductViewsSum($params)
  {
    $views = $this->init()
                  ->select('id')
                  ->withCount([
                    'viewsCount as views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                    },
                    'viewsCount as auth_user_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->whereNotNull('user_id');
                    },
                    'viewsCount as auth_user_has_shop_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_shop', 1);
                    },
                    'viewsCount as auth_user_has_ad_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_ad', 1);
                    },
                    'viewsCount as auth_user_has_lot_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_lot', 1);
                    }
                  ])
                  ->get();
    
    return $views;
  }

  public function getProductsViewsByShop(int $shop_id, $params)
  {
    $model = $this->init()
                  ->select([
                    'id',
                    'store_id'
                  ])
                  ->where('store_id', $shop_id)
                  ->withCount([
                    'viewsCount as views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                    },
                    'viewsCount as auth_user_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->whereNotNull('user_id');
                    },
                    'viewsCount as auth_user_has_shop_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_shop', 1);
                    },
                    'viewsCount as auth_user_has_ad_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_ad', 1);
                    },
                    'viewsCount as auth_user_has_lot_views' => function($query) use($params){
                      if (isset($params['from_date'])) {
                        $query->whereDate('created_at', '>=', $params['from_date']);
                      }
                      if (isset($params['to_date'])) {
                        $query->whereDate('created_at', '<=', $params['to_date']);
                      }
                      $query->where('has_lot', 1);
                    }
                  ])
                  ->get();
    return $model;
  }

}