<?php

namespace App\Traits;

trait SendSuccessResponseTrait
{
  public function successResponse($data = [], $status_c = 200)
  {
    return response()->json([
      'data' => $data,
      'success' => true,
      'error' => false,
      'status_code' => $status_c
    ], $status_c);
  }
}
