<?php

namespace App\Traits;

/**
 *
 */
trait UploadFileTrait
{

    /**
     * upload_file
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $filename (avatar, logotype)
     * @param  string $path Path where save file
     *
     * @return string path to file
     */
    public function upload_file($request, $filename, $path)
    {
      $path = $request->file($filename)->store('/' . $path, 'uploads');
      return '/uploads/' . $path;
    }
}
