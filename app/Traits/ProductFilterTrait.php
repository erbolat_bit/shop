<?php

namespace App\Traits;

use App\Currency\Convert;
use App\Models\Product;

trait ProductFilterTrait
{

  public function params($products)
  {
    $manufacturer_products_count = $products->groupBy('manufacturer_id')->transform(function ($item, $key) {
      return [
        'id' => $key,
        'products_count' => $item->count()
      ];
    })->toArray();

    $category_products_count = $products->groupBy('category_id')->transform(function ($item, $key) {
      return [
        'id' => $key,
        'products_count' => $item->count()
      ];
    })->toArray();
    $params = $products->transform(function ($item, $key) {
      $result = $item->params->unique('option_id')->pluck('option_id', 'parameter_id');
      return $result;
    })->toArray();
    $params = array_filter($params, fn ($value) => !empty($value) && $value !== '');
    $parameter_ids = [];
    foreach ($params as $key => $param) {
      foreach ($param as $p_key => $option) {
        if ($option) {
          $parameter_ids[$p_key][] = $option;
        }
      }
    }
    $min_max_price = $this->min_max_price();
    $products_count = $products->count();

    // масив параметры который есть продукты
    // 26 => [99, 99, 100, 99] тут 26 это parameter_id а [99, 99, 100, 99] option_ids и можно рассчитать количество продуктов у параметра
    // пример у параметра 99 есть 3 продукта
    // $parameter_ids = [  
    //   5 => [26, 24],
    //   6 => [6],
    //   7 => [18, 19],
    //   9 => [46, 291, 292],
    //   10 => [47, 47, 47, 47],
    //   12 => [49],
    //   26 => [99, 99, 100, 99],
    //   27 => [111, 109],
    //   28 => [115, 115, 115, 115],
    //   29 => [117, 117, 117, 117],
    //   30 => [122, 122, 122],
    //   31 => [123, 119, 123],
    //   32 => [182, 278, 278, 276],
    //   33 => [204],
    //   34 => [205],
    //   35 => [206, 206, 206],
    //   53 => [277, 277, 277],
    //   54 => [279, 285, 280],
    // ];
    return [
      'manufacturer_products_count' => array_values($manufacturer_products_count),
      'category_products_count' => array_values($category_products_count),
      'min_max_price' => $min_max_price,
      'products_count' => $products_count,
      'parameter_ids' => $parameter_ids
    ];
  }

  public function min_max_price()
  {
    $money = new Convert();
    $products = Product::get();
    $products = $products->transform(function ($item, $key) use ($money) {
      $item->price = $money->fromTo($item->price, $item->currency(), config('app.locale_currency'))->format(true);
      return $item;
    });

    return [$products->min('price'), $products->max('price')];
  }

  public function filter($request, $products)
  {

    if ($request->q) {
      $products->where('title', 'like', '%' . $request->q . '%');
    }
    info($request->input());
    if ($request->child_categories_id && !$request->has('category_id')) {
      info($request->child_categories_id);
      $products->whereIn('category_id', $request->child_categories_id);
    }

    if ($request->category_id && !$request->has('child_categories_id')) {
      if (is_array($request->category_id)) {
        $products->whereIn('category_id', $request->category_id);
      } else {
        $products->where('category_id', $request->category_id);
      }
    }

    if ($request->manufacturer_id) {
      $products->whereIn('manufacturer_id', explode(',', $request->manufacturer_id));
    }

    if ($request->country_id) {
      $products->whereIn('country_id', explode(',', $request->country_id));
    }

    if ($request->params) {
      $params = explode(',', $request->params);
      $products->whereHas('params', function ($query) use ($params) {
        $query->whereIn('option_id', $params);
      });
    }
    if ($request->sort) {
      $desc = $request->desc == 'true' ? 'desc' : 'asc';
      if ($request->sort == 'order') {
        $products->orderBy('title', $desc);
      }

      if ($request->sort == 'price') {
        $products->orderBy('price', $desc);
      }

      if ($request->sort == 'popular') {
        $products->popular($desc);
      }

      if ($request->sort == 'novelty') {
        $products->orderBy('created_at', $desc);
      }
    } else {
      $products->orderBy('created_at', 'desc');
    }

    $money = new Convert();
    $products = $products->get();

    if ($request->min_price) {
      $products = $products->filter(function ($item, $key) use ($request, $money) {
        $min_price = $money->fromTo($request->min_price, $request->header('Currency'), $item->currency())->format(true);
        return $item->price >= $min_price;
      });
    }

    if ($request->max_price) {
      $products = $products->filter(function ($item, $key) use ($request, $money) {
        $max_price = $money->fromTo($request->max_price, $request->header('Currency'), $item->currency())->format(true);
        return $item->price <= $max_price;
      });
    }

    return $products;
  }
}
