<?php

namespace App\Traits;

use App\Models\ProductParam;
use App\Models\ProductPhoto;
use App\Models\ProductTag;

/**
 * 
 */
trait ProductHelperTrait
{
  private function updateAddData($request, $product)
  {
    $photos = $this->preparationSavePhotos($request, $product);
    $product->photos()->saveMany($photos);
    $this->saveOrUpdateTags($request->tags, $product);
    $this->saveParams($request->param_options, $product);
  }

  private function preparationSavePhotos($request, $product)
  {
    $photos = [];
    if ($request->file('photos')) {
      foreach ($request->file('photos') as $key => $photo) {
        $path = $this->upload_file($request, 'photos.' . $key . '.photo', 'product/photos');
        $photos[$key] = new ProductPhoto([
          'photo' => $path
        ]);
      }
    }

    return $photos;
  }

  private function saveOrUpdateTags($tags, $product)
  {
    $product->tags()->delete();
    if (!empty($tags)) {
      $tags_o = [];
      foreach ($tags as $key => $value) {
        $tags_o[] = new ProductTag(['title' => $value['title']]);
      }
      $product->tags()->saveMany($tags_o);
    }
  }

  private function saveParams($param_options, $product)
  {
    $product->params()->delete();

    if (!empty($param_options)) {
      $options = [];
      foreach ($param_options as $key => $value) {
        $options[$key] = new ProductParam($value);
      }
      $product->params()->saveMany($options);
    }
  }
}
