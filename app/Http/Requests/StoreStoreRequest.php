<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'logotype' => 'nullable|image|mimes:jpeg,bmp,png|max:1024',
            'title' => 'required|string',
            'description' => 'nullable|string',
            'type' => 'required|integer',
            'subdomain' => 'nullable|string|unique:stores',
            'delivery.*.id' => 'nullable|integer',
            'delivery.*.title' => 'nullable|string',
            'data.*.title' => 'nullable|string',
            'data.*.category' => 'nullable|string',
            'data.*.main' => 'nullable|integer',
        ];
    }
}
