<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        
        return [
            'category_id' => 'required|integer',
            'title' => 'required|string|max:255',
            'price' => 'required|integer',
            'discount_price' => 'nullable|integer',
            'currency_id' => 'required|integer|in:'.implode(',', array_keys(config('params.currencies'))),
            'quantity' => 'required|integer',
            'quantity_size' => 'required|integer|in:'.implode(',', array_keys(config('params.quantity_size'))),
            'delivery_description' => 'nullable|string',
            'photos.*.photo' => 'nullable|image|mimes:jpeg,bmp,png|max:1024',
            'description' => 'nullable|string',
            'country_id' => 'required|integer',
            'manufacturer_id' => 'required|integer',
            'detailed_description' => 'nullable|string',
            'tags' => 'nullable|array',
            'param_options.*.option_id' => 'nullable|integer',
            'param_options.*.value' => 'nullable|string',
            'brand_id' => 'nullable|integer',
        ];
    }
}
