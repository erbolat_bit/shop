<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class UpdateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'logotype' => 'nullable|image|mimes:jpeg,bmp,png|max:1024',
            'title' => 'nullable|string',
            'description' => 'nullable|string',
            'type' => 'nullable|integer',
            'subdomain' => [
                  'nullable',
                  'string',
                  Rule::unique('stores')->ignore($request->store->id, 'id')
              ],
            'delivery' => 'nullable|array',
            'bg_color' => 'nullable|array',
            'data.*.title' => 'nullable|string',
            'data.*.category' => 'nullable|string',
            'data.*.main' => 'nullable|integer',
            'bg_image' => 'nullable|image|mimes:jpeg,bmp,png|max:1024'
        ];
    }
}
