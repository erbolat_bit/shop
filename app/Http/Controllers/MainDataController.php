<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Store;
use App\Transformers\ProductTransformer;
use App\Transformers\StoreTransforemer;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\DataArraySerializer;

class MainDataController extends Controller{


  public function index(Request $request)
  {
    $result = [];
    $product_paginator = Product::orderBy('created_at', 'desc')->paginate(4);
    $products = $product_paginator->getCollection();

    $products = fractal()
            ->collection($products, new ProductTransformer, 'data')
            ->paginateWith(new IlluminatePaginatorAdapter($product_paginator))
            ->serializeWith(new DataArraySerializer);

    $store_paginator = Store::orderBy('created_at', 'desc')->paginate(4);
    $stores = $store_paginator->getCollection();

    $stores = fractal()
            ->collection($stores, new StoreTransforemer, 'data')
            ->paginateWith(new IlluminatePaginatorAdapter($store_paginator))
            ->serializeWith(new DataArraySerializer);
            
    $result[1] = [
      'title' => 'Самое новое',
      'type' => 'product',
      'products' => $products,
    ];
    $result[2] = [
      'title' => 'Магазины',
      'type' => 'store',
      'products' => $stores,
    ];

    return fractal()
        ->item($result, function($data){
          return $data;
        })
        ->serializeWith(new DataArraySerializer);
  }
}