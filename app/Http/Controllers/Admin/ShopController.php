<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Store as ResourcesStore;
use App\Models\Store;
use App\Services\ViewsService;
use Illuminate\Http\Request;
use App\Transformers\StoreTransforemer;
use App\Transformers\AdminStoreTransforemer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\DataArraySerializer;
use App\Traits\SendSuccessResponseTrait;
use App\Transformers\AdminUpdateStoreTransformer;
use App\Traits\UploadFileTrait;
use Illuminate\Validation\Rule;

class ShopController extends Controller
{
  use SendSuccessResponseTrait;
  use UploadFileTrait;

  private $viewsService;

  public function __construct(ViewsService $viewsService)
  {
    $this->viewsService = $viewsService;
  }

  /**
   * Display the list of resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    if ($request->has('paginate') && !$request->paginate) {
      $shop = Store::select(['id', 'title'])->get()->toArray();
      return response()->json([
        'data' => $shop,
        'success' => true,
        'error' => false,
        'status_code' => 200
      ]);
    }else{
      $paginator = Store::when($request->has('status'), function ($query) use ($request) {
        return $query->where('status', $request->status);
      })->orderBy('created_at', 'desc')->paginate(10);
      return ResourcesStore::collection($paginator)->additional([
        'success' => true,
        'error' => false,
        'status_code' => 200
      ]);
    }

  }

  /**
   * Filtering data.
   *
   * @return \Illuminate\Http\Response
   */
  public function filter(Request $request)
  {

    $filterabels = $request->except(['per_page', 'order_by', 'title', 'subdomain', 'order_by', 'asc']);

    $query = Store::orderBy($request->order_by ?? 'created_at', $request->asc ?? 'desc');

    foreach ($filterabels as $key => $val) {
      if (is_null($val)) continue;
      $query = $query->where($key, $val);
    }

    if ($request->has('title'))
      $query = $query->where('title', 'like', '%' . $request->title . '%');

    if ($request->has('subdomain'))
      $query = $query->where('subdomain', 'like', '%' . $request->subdomain . '%');

    $paginator = $query->paginate($request->per_page ?? 15);

    $store = $paginator->getCollection();

    return fractal()
      ->collection($store, new StoreTransforemer, 'data')
      ->paginateWith(new IlluminatePaginatorAdapter($paginator))
      ->serializeWith(new DataArraySerializer);
  }


  /**
   * Display the specific resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id)
  {
    $store = Store::where('id', $id)->withCount('products as product_count')->first();
    $res =  fractal()
      ->item($store)
      ->transformWith(new AdminStoreTransforemer)
      ->toArray();
    return $this->successResponse($res);
  }
  /**
   * Get Ids and Responde with title of the store
   * @return \Illuminate\Http\Response
   */
  public function idsToTitles(Request $request)
  {
    $request->validate([
      'store_ids' => 'required|array'
    ]);
    $stores = Store::find($request->store_ids)->pluck('id', 'title');
    return $this->successResponse($stores);
  }

  //* Shop Detials for updating
  public function shopDetails(Request $request, $id)
  {
    $shop = Store::find($id);
    $res =  fractal()
      ->item($shop)
      ->transformWith(new AdminUpdateStoreTransformer)
      ->toArray();
    return $this->successResponse($res);
  }

  //* Update shop as Admin
  public function update(Request $request, Store $store)
  {
    $request->validate([
      'bg_image' => 'nullable|image|mimes:png,jpg,jpeg',
      'logotype' => 'nullable|image|mimes:png,jpg,jpeg',
      'type' => Rule::in(array_keys(config('params.store_types'))),
      'title' => 'required|string',
      'description' => 'nullable|string',
      'subdomain' => 'nullable|string',
      'status' => 'nullable|string'
    ]);
    $data = $request->except(['bg_image', 'logotype', 'status']);
    if ($request->hasFile('logotype'))
      $data['logotype'] = $this->upload_file($request, 'logotype', 'store/logotype');
    if ($request->hasFile('bg_image'))
      $data['bg_image'] = $this->upload_file($request, 'bg_image', 'store/bg');
    if ($request->has('status')) {
      $data['status'] = true;
    } else {
      $data['status'] = false;
    }
    $store->update($data);
    return $this->successResponse($store->toArray());
  }

  //* Delete Store As Admin
  public function delete(Request $request, Store $store)
  {
    $store_details = $store->toArray();
    $store->delete();
    return $this->successResponse($store_details);
  }

  public function getShopViews(Request $request, $id=null)
  {
    $data = $this->viewsService->getViewStatistic($id, $request->input());
    return $data;
  }

}
