<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Product as ResourcesProduct;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Transformers\ProductTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\DataArraySerializer;
use App\Traits\SendSuccessResponseTrait;

class ProductController extends Controller
{
  use SendSuccessResponseTrait;
  /**
   * Display the list of resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $pagination = Product::with('photos')->with('store')->when($request->has('store_id'), function ($query) use ($request) {
      return $query->where('products.store_id', $request->store_id);
    })->paginate(10);
    return ResourcesProduct::collection($pagination)->additional([
      'success' => true,
      'error' => false,
      'status_code' => 200
    ]);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Product  $product
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $id)
  {
    $product = Product::where('id', $id)->with(['photos', 'docs', 'params', 'store'])->first();
    $product = transform($product, function ($obj) {
      $obj->currency = config('params.currencies.' . $obj->currency_id);
      $obj->quantity_size = config('params.quantity_size.' . $obj->quantity_size);
      return $obj;
    });
    return $this->successResponse($product);
  }

  /**
   * Filtering data.
   *
   * @return \Illuminate\Http\Response
   */
  public function filter(Request $request)
  {
    $filterabels = $request->except(['per_page', 'title', 'order_by', 'asc']);

    $query = Product::orderBy($request->order_by ?? 'created_at', $request->asc ?? 'desc');

    foreach ($filterabels as $key => $val) {
      if (is_null($val)) continue;
      $query = $query->where($key, $val);
    }

    if ($request->has('title'))
      $query = $query->where('title', 'like', '%' . $request->title . '%');

    $paginator = $query->paginate($request->per_page ?? 15);

    $products = $paginator->getCollection();

    return fractal()
      ->collection($products, new ProductTransformer)
      ->paginateWith(new IlluminatePaginatorAdapter($paginator))
      ->serializeWith(new DataArraySerializer);
  }

  /**
   * Get Ids and Responde with title of the product
   * @return \Illuminate\Http\Response
   */
  public function idsToNames(Request $request)
  {
    $request->validate([
      'product_ids' => 'required|array'
    ]);
    $products = Product::find($request->product_ids)->pluck('id', 'title');
    return $this->successResponse($products);
  }


  //* Update product details
  public function update(Request $request, Product $product)
  {
    $data = $request->only(['title', 'description', 'delivery_description', 'detailed_description']);
    $request->validate([
      'title' => 'required|string',
      'description' => 'nullable|string',
      'delivery_description' => 'nullable|string',
      'detailed_description' => 'nullable|string'
    ]);
    $product->update($data);
    return $this->successResponse($product->toArray());
  }

  //* Delete Product
  public function delete(Request $request, Product $product)
  {
    $product_details = $product->toArray();
    $product->delete();
    return $this->successResponse($product_details);
  }
}
