<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStoreRequest;
use App\Http\Requests\UpdateStoreRequest;
use App\Models\Store;
use App\Models\StoreAddData;
use App\Models\View;
use App\Traits\UploadFileTrait;
use App\Transformers\StoreTransforemer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\DataArraySerializer;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StoreController extends Controller
{

  use UploadFileTrait;

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $user_id = $request->user_id;
    if ($user_id) {
      $paginator = Store::where('user_id', $user_id)->withCount('viewsCount as views')->orderBy('created_at', 'desc')->paginate($request->per_page ? $request->per_page : 15);
    } else {
      $paginator = Store::orderBy('created_at', 'desc')->withCount('viewsCount as views')->paginate($request->per_page ? $request->per_page : 15);
    }
    $stores = $paginator->getCollection();

    return fractal()
      ->collection($stores, new StoreTransforemer, 'data')
      ->paginateWith(new IlluminatePaginatorAdapter($paginator))
      ->serializeWith(new DataArraySerializer);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreStoreRequest $request)
  {

    $store = DB::transaction(function () use ($request) {
      $input = $request->toArray();
      $input['user_id'] = $request->user_id ?? 4;
      $input['subdomain'] = $request->subdomain ?? '@' . str_replace('-', '', Str::slug($request->title, '-'));

      if ($request->hasFile('logotype')) {
        $input['logotype'] = $this->upload_file($request, 'logotype', 'store/logotype');
      }

      $store = new Store($input);
      $data = [];
      if ($request->data) {
        foreach ($request->data as $key => $value) {
          $data[$key] = new StoreAddData($value);
        }
      }

      $store->save();
      if (!empty($data)) {
        $store->addData()->saveMany($data);
      }

      return $store;
    });
    return fractal()
      ->item($store)
      ->transformWith(new StoreTransforemer)
      ->toArray();
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Store  $store
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $store)
  {
    $store = Store::where('id', $store)->withCount('viewsCount as views')->first();
    return fractal()
      ->item($store)
      ->transformWith(new StoreTransforemer)
      ->toArray();
  }


  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Store  $store
   * @return \Illuminate\Http\Response
   */
  public function getByDomain(Request $request, $domain)
  {
    $store = Store::where('subdomain', $domain)->withCount('viewsCount as views')->first();

    if (!$store)
      throw new NotFoundHttpException();

    return fractal()
      ->item($store)
      ->transformWith(new StoreTransforemer)
      ->toArray();
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Store  $store
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateStoreRequest $request, Store $store)
  {

    if ($store->user_id != $request->user_id)
      throw new ModelNotFoundException();

    $store = DB::transaction(function () use ($request, $store) {

      if ($request->data) {
        $store->addData()->delete();
      }

      $input = $request->toArray();

      if ($request->hasFile('logotype')) {
        $input['logotype'] = $this->upload_file($request, 'logotype', 'store/logotype');
      }
      if ($request->hasFile('bg_image')) {
        $input['bg_image'] = $this->upload_file($request, 'bg_image', 'store/bg');
      }

      $store->update($input);

      $data = [];
      if ($request->data) {
        foreach ($request->data as $key => $value) {
          $data[$key] = new StoreAddData($value);
        }
      }

      $store->addData()->saveMany($data);

      return $store;
    });
    return fractal()
      ->item($store)
      ->transformWith(new StoreTransforemer)
      ->toArray();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Store  $store
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, Store $store)
  {
    if ($store->user_id != $request->user_id)
      throw new ModelNotFoundException();

    $store->delete();
    return fractal()
      ->item($store)
      ->transformWith(new StoreTransforemer)
      ->toArray();
  }

  public function has($user_id)
  {
    $shop = Store::where('user_id', $user_id)->first();
    if ($shop) {
      return response()->json(true);
    }

    return response()->json(false);
  }

  /**
   * Remove the Background Image.
   *
   * @param  \App\Models\Api\User\Store\Store  $store
   * @return \Illuminate\Http\Response
   */
  public function deleteBgImage(Request $request, Store $store)
  {
    if ($store->user_id != $request->user_id)
      throw new ModelNotFoundException();

    File::delete(public_path($store->bg_image));
    $store->update(['bg_image' => null]);
    return fractal()
      ->item($store)
      ->transformWith(new StoreTransforemer)
      ->toArray();
  }



  /**
   * @group Store
   * Set view to product
   * @urlParam product required
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function viewStore(Request $request, Store $store)
  {
    if ($request->user_id) {
      $view = new View([
        'user_id' => $request->user_id,
        'view' => 1,
        'has_shop' => (bool)$request->has_shop,
        'has_ad' => (bool)$request->has_ad,
        'has_lot' => (bool)$request->has_lot,
      ]);
    }else{
      $view = new View();
    }
    $store->viewsCount()->save($view);
    return fractal()
      ->item($store)
      ->transformWith(new StoreTransforemer)
      ->toArray();
  }

  public function getUserIdsByShopIds(Request $request)
  {
    $request->validate([
      'store_ids' => 'required|array'
    ]);

    $user_ids = Store::whereIn('id', $request->store_ids)->get(['user_id', 'id as store_id'])->toArray();
    return response()->json($user_ids);
  }
}
