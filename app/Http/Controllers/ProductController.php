<?php

namespace App\Http\Controllers;

use App\Http\Requests\DocumentUploadRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use App\Models\ProductDoc;
use App\Models\Store;
use App\Models\ProductPhoto;
use App\Models\View;
use App\Traits\UploadFileTrait;
use App\Transformers\DocumentTransformer;
use App\Transformers\ProductTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\DataArraySerializer;
use App\Traits\ProductFilterTrait;
use App\Traits\ProductHelperTrait;
use App\Transformers\ProductSearchTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductController extends Controller
{

  use UploadFileTrait, ProductFilterTrait, ProductHelperTrait;

  public function index(Request $request, Store $store)
  {
    $paginator = $store->products()->withCount('viewsCount as views')->orderBy('created_at', 'desc')->paginate();
    $products = $paginator->getCollection();

    return fractal()
      ->collection($products, new ProductTransformer($request), 'data')
      ->paginateWith(new IlluminatePaginatorAdapter($paginator))
      ->serializeWith(new DataArraySerializer);
  }

  public function getAllProductByStoreId(Request $request, Store $store)
  {
    $products = $store->products()->withCount('viewsCount as views')->orderBy('created_at', 'desc')->get();

    return fractal()
      ->collection($products)
      ->transformWith(new ProductTransformer($request))
      ->toArray();
  }

  public function getProducts(Request $request)
  {
    if ($request->has('category_id') || $request->has('child_categories_id') || $request->has('q') || $request->has('manufacturer_id') || $request->has('params') || $request->has('sort')) {
      $products = Product::with(['params', 'docs', 'tags', 'photos']);
      $filter_products = $this->filter($request, $products);

      $product = collect([
        'products' => $filter_products->paginate()->getCollection(),
        'pagination' => $filter_products->paginate(),
        'params' => $request->has('main') && $request->main ? [] : $this->params($filter_products),
      ]);

      return fractal()
        ->item($product, new ProductSearchTransformer($request))
        ->toArray();
    } else {
      $paginator = Product::orderBy('created_at', 'desc');
      $paginator = $paginator->paginate($request->per_page ? $request->per_page : 15);
      $products = $paginator->getCollection();

      return fractal()
        ->collection($products, new ProductTransformer($request), 'data')
        ->paginateWith(new IlluminatePaginatorAdapter($paginator))
        ->serializeWith(new DataArraySerializer);
    }
  }

  public function basketProducts(Request $request)
  {
    $request->validate([
      'products.*' => 'required|integer'
    ]);
    
    $products = Product::orderBy('created_at', 'desc')
      ->whereIn('id', $request->products)
      ->get();

    return fractal()
      ->collection($products)
      ->transformWith(new ProductTransformer())
      ->parseIncludes('store')
      ->toArray();
  }

  public function store(StoreProductRequest $request, Store $shop)
  {

    if ($shop->user_id != $request->user_id)
      throw new ModelNotFoundException();

    $product = DB::transaction(function () use ($request, $shop) {
      $product = new Product($request->input());
      $shop->products()->save($product);

      $photos = $this->preparationSavePhotos($request, $product);
      $product->photos()->saveMany($photos);

      $this->saveOrUpdateTags($request->tags, $product);

      $this->saveParams($request->param_options, $product);

      return $product;
    });

    return fractal()
      ->item($product)
      ->transformWith(new ProductTransformer($request))
      ->toArray();
  }

  public function show(Request $request, $product)
  {
    $product = Product::where('id', $product)->withCount('viewsCount as views')->first();
    return fractal()
      ->item($product)
      ->transformWith(new ProductTransformer($request))
      ->parseIncludes('store')
      ->toArray();
  }

  public function update(UpdateProductRequest $request, Product $product)
  {
    if ($product->store->user_id != $request->user_id)
      throw new ModelNotFoundException();

    $product->update($request->toArray());
    $this->updateAddData($request, $product);

    return fractal()
      ->item($product)
      ->transformWith(new ProductTransformer($request))
      ->toArray();
  }

  public function viewsProduct(Request $request, Product $product)
  {
    if ($request->user_id) {
        $view = new View([
          'user_id' => $request->user_id,
          'view' => 1,
          'has_shop' => $request->has_shop,
          'has_ad' => $request->has_ad,
          'has_lot' => $request->has_lot,
        ]);
    }else{
      $view = new View();
    }
    $product->viewsCount()->save($view);

    return fractal()
      ->item($product)
      ->transformWith(new ProductTransformer($request))
      ->toArray();
  }

  public function document(DocumentUploadRequest $request, Product $product)
  {

    if ($product->store->user_id != $request->user_id)
      throw new ModelNotFoundException();

    $document = new ProductDoc([
      'title' => $request->title,
      'document' => $this->upload_file($request, 'document', 'lot/document'),
      'size' => $request->size
    ]);
    $product->docs()->save($document);

    return fractal()
      ->item($document)
      ->transformWith(new DocumentTransformer)
      ->toArray();
  }

  public function deleteDocument(Request $request, ProductDoc $document)
  {

    if ($document->product->store->user_id != $request->user_id)
      throw new ModelNotFoundException();

    $document->delete();
    return fractal()
      ->item($document)
      ->transformWith(new DocumentTransformer)
      ->toArray();
  }

  public function destroy(Request $request, Product $product)
  {
    if ($product->store->user_id != $request->user_id)
      throw new ModelNotFoundException();

    $product->delete();
    return fractal()
      ->item($product)
      ->transformWith(new ProductTransformer($request))
      ->toArray();
  }

  public function deletePhoto(Request $request, ProductPhoto $photo)
  {
    if ($photo->product->store->user_id != $request->user_id)
      throw new ModelNotFoundException();

    $photo->delete();
    return fractal()
      ->item($photo, function () use ($photo) {
        return $photo->toArray();
      })->toArray();
  }
}
