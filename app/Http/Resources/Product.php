<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'store_title' => $this->store_title,
      'category_id' => $this->category_id,
      'title' => $this->title,
      'price' => $this->price,
      'currency' => config('params.currencies.' . $this->currency_id),
      'country_id' => $this->country_id,
      'quantity' => $this->quantity,
      'views' => $this->views,
      'photos' => $this->photos,
      'store' => $this->store
    ];
  }
}
