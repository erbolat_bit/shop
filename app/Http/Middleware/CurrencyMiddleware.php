<?php

namespace App\Http\Middleware;

use Closure;

class CurrencyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('Currency')){
            config(['app.locale_currency' => $request->header('Currency')]);
        }
        return $next($request);
    }
}
