<?php

namespace App\Http\Middleware;

use Closure;

class ClientAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->headers->has('X-CLIENT-TOKEN') && $request->header('X-CLIENT-TOKEN') == config('auth.client_token')) {
            return $next($request);
        }else{
            abort(401);
        }
    }
}
