<?php

namespace App\Currency;

use App\Invoke\CbuCurrencies;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use NumberFormatter;

class Convert
{
  
  private $amount;
  private $from; // currency from database
  private $to;
  private $currencies = [];

  public function __construct()
  {
    try {
      if(!Storage::exists('currency/currencies.json')){
        $currnecy = new CbuCurrencies;
        $currnecy();
      }
     
      $this->currencies = json_decode(Storage::get('currency/currencies.json'), true);
    } catch (\Throwable $th) {
      throw $th;
    }
  }

  /**
   * Convert from (currency USD) to (currency RUB)
   * 
   * @param  mixed $amount
   * @param  string $from
   * @param  string $to
   * @return self
   */
  public function fromTo($amount, $from, $to){

    $this->from = $from;
    $this->to = $to;

    $this->amount = $this->convert($amount);

    return $this;
  }

  /**
   * Format amount
   * 
   * @param  bool $number
   * @param  string $format (de_DE, ru_RU)
   * @return mixed NumberFormatter or number
   */
  public function format($number=true, $format='ru_RU'){
    
    if(!$number){
      $fmt = new NumberFormatter( $format, NumberFormatter::CURRENCY );
      return $fmt->formatCurrency($this->amount, $this->to);
    }
    return $this->amount;
  }
  
  /**
   * Find currency data in currencies by currency
   *
   * @param  mixed $currency (USD, UZS)
   * @return array Currency
   */
  public function findCurrency($currency){

    $currency = Arr::where($this->currencies, function($value, $key) use ($currency){
        return $value['Ccy'] ===  strtoupper($currency);
    });
    return Arr::collapse($currency);
  }
  
  /**
   * Convert
   *
   * @param  number $amount
   * @return float
   */
  private function convert($amount){
    
    $from = $this->findCurrency($this->from);
    $to = $this->findCurrency($this->to);

    if($this->from == $this->to)
      return $amount;
    
    if($this->from != 'UZS' && $this->to == 'UZS'){
      $result = $this->numberFormat($this->numberFormat($amount) * $this->numberFormat($from['Rate']));
    }

    if ($this->from == 'UZS' && $this->to != 'UZS') {
      $result = $this->numberFormat($this->numberFormat($amount) / $this->numberFormat($to['Rate']));
    }

    if($this->from != 'UZS' && $this->to != 'UZS'){
      $uzs = $this->numberFormat($this->numberFormat($amount) * $this->numberFormat($from['Rate']));
      $result = $this->numberFormat($this->numberFormat($uzs) / $this->numberFormat($to['Rate']));
    }

    return $result;
  }

  public function numberFormat($number){
    return number_format($number, 2, '.', '');
  }

}
