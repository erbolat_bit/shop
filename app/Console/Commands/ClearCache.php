<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearCache extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'clear:server-cache';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Clear Nginx server cache';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    exec('rm -rf /var/run/nginx-fastcgi-cache/*');
    // exec('rm -rf /var/www/test/*');
  }
}
