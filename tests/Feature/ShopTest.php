<?php

namespace Tests\Feature;

use App\Repositories\ShopRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShopTest extends TestCase
{

  
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetAllShopViewsSum()
    {
        $response = $this->get('/admin/shop/all-views');
        $response->assertStatus(200);
    }

    public function testGetShopViewsByUsers()
    {
        $user_ids = range(0, 100);
        $user_ids = [$user_ids[rand(0,100)], $user_ids[rand(0,100)], $user_ids[rand(0,100)]];
        $response = $this->get('/admin/shop/views/' . rand(0,100) . '?' . http_build_query(['user_ids' => $user_ids]));
        
        $response->assertStatus(200);
    }
}
