<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([
  // 'middleware' => 'clientAuth'
], function () {

  Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin'
  ], function () {

    Route::group(['prefix' => 'shop'], function () {
      Route::get('list', 'ShopController@index');
      Route::get('/names', 'ShopController@idsToTitles');
      Route::get('show/{id}', 'ShopController@show')->where(['store' => '[0-9]+']);
      Route::get('show', 'ShopController@filter');
      Route::get('details/{id}', 'ShopController@shopDetails');
      Route::post('update/{store}', 'ShopController@update');
      Route::delete('delete/{store}', 'ShopController@delete');

      Route::get('all-views', 'ShopController@allViews');
      Route::get('views/{id?}', 'ShopController@getShopViews');
    });

    Route::group(['prefix' => 'product'], function () {
      Route::get('/names', 'ProductController@idsToNames');
      Route::get('list', 'ProductController@index');
      Route::get('show/{id}', 'ProductController@show')->where(['id' => '[0-9]+']);
      Route::get('show', 'ProductController@filter');
      Route::post('/update/{product}', 'ProductController@update');
      Route::delete('/delete/{product}', 'ProductController@delete');
    });
  });

  Route::group(['prefix' => 'shop'], function () {

    Route::get('user/stores', 'StoreController@index');
    Route::get('has/{user_id}', 'StoreController@has');
    Route::post('store', 'StoreController@store');
    Route::post('/{store}', 'StoreController@update');
    Route::delete('/{store}', 'StoreController@destroy');
    Route::delete('bg-image/{store}', 'StoreController@deleteBgImage');

    Route::get('/', 'StoreController@index');
    Route::get('/{store}', 'StoreController@show');
    Route::get('store-by-domain/{domain}', 'StoreController@getByDomain');
    Route::post('views/{store}', 'StoreController@viewStore');
    Route::get('user/ids', 'StoreController@getUserIdsByShopIds');
  });

  Route::group(['prefix' => 'product'], function () {
    Route::post('store/{shop}', 'ProductController@store');
    Route::post('update/{product}', 'ProductController@update');
    Route::post('document/{product}', 'ProductController@document');
    Route::delete('document/{document}', 'ProductController@deleteDocument');
    Route::delete('/{product}', 'ProductController@destroy');
    Route::delete('photo/{photo}', 'ProductController@deletePhoto');
    Route::get('all-by-shop/{store}', 'ProductController@getAllProductByStoreId');
  });



  Route::group(['prefix' => 'product'], function () {

    Route::get('by-shop/{store}', 'ProductController@index');
    Route::get('show/{product}', 'ProductController@show');
    Route::get('all', 'ProductController@getProducts');
    Route::post('views/{product}', 'ProductController@viewsProduct');
    Route::get('basket', 'ProductController@basketProducts');
    Route::get('wishlist', 'ProductController@basketProducts');
  });

  Route::group(['prefix' => 'main-data'], function () {

    Route::get('/', 'MainDataController@index');
  });
});
